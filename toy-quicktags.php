<?php

/**
 * Plugin Name:       Toy QuickTags
 * Plugin URI:        https://gitlab.com/67656e65726963/toy-quicktags
 * Description:       Adds quicktags for span, small, and hr HTML elements.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action(
	'admin_print_footer_scripts',
	function() {
		// Add buttons to html editor
		if ( wp_script_is( 'quicktags' ) ) : ?>
			<script>
				QTags.addButton( 'eg_span', 'span', '<span>', '</span>', 'span', 'Span', 67 );
				QTags.addButton( 'eg_small', 'small', '<small>', '</small>', 'small', 'Small', 67 );
				QTags.addButton( 'eg_hr', 'hr', '<hr />', '', 'h', 'Horizontal rule line', 67 );
			</script>
			<?php
		endif;
	},
	67
);
